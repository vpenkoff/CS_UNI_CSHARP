using System;
using System.Threading;

namespace Task2
{
	class ExcessSpeedEvent : System.EventArgs
	{
		private int excess_speed;
		public int Ex_Speed
		{
			get { return excess_speed; }
			set { excess_speed = value; }
		}

		public ExcessSpeedEvent(int speed)
		{
			excess_speed = speed;
		}
	}

	class Car
	{
		private int speed;
		private int safety_speed;

		public Car(int sp, int sfp)
		{
			this.speed = sp;
			this.safety_speed = sfp;
		}

		public int Speed
		{
			get { return speed; }
			set { speed = value; }
		}
		public int Safe_Speed
		{
			get { return safety_speed; }
			set {safety_speed = value; }
		}

		public delegate void ExcessSpeedEventHandler(object source, ExcessSpeedEvent e);
		public event ExcessSpeedEventHandler OnSpeed;

		public void Accelerate (int accelerate)
		{
			int excess_speed;
			Speed += accelerate;
			excess_speed = Speed - Safe_Speed;
			if (Speed < Safe_Speed)
				return;

			ExcessSpeedEvent e = new ExcessSpeedEvent (excess_speed);

			if (OnSpeed != null)
				OnSpeed (this, e);
		}
	}

	class Traffic
	{
		private Car car;
		public Traffic(Car c)
		{
			this.car = c;
			car.OnSpeed += new Car.ExcessSpeedEventHandler (OnSpeedWarning);
		}

		void OnSpeedWarning(object source, ExcessSpeedEvent e)
		{
			if (source == null)
				return;
			Car c = source as Car;
			Console.WriteLine ("Speed {0}, ExcessSpeed {1}. Warning!!!",c.Speed, e.Ex_Speed );
		}
	}

	class MainClass
	{
		public static void Main (string[] args)
		{
			Car c = new Car (5, 80);
			Traffic tr = new Traffic (c);


			for (int i = 0; i < 300; i = i + 10) {
					Console.WriteLine ("{0}", c.Speed);
				c.Accelerate (10);
				Console.ReadLine ();
			}
		}
	}
}
