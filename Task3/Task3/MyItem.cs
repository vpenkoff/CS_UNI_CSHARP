using System;
using Task3;

namespace Task3
{
	public class MyItem
	{
		int item_id;
		string item_descr;
		double item_cost;

		public MyItem(int id, string descr, double cost)
		{
			item_id = id;
			item_descr = descr;
			item_cost = cost;
		}

		public int ID
		{
			get { return this.item_id; }
			set {this.item_id = value; }
		}

		public string DESCR
		{
			get { return this.item_descr; }
		}

		public double COST
		{
			get { return this.item_cost; }
		}
	}
}

