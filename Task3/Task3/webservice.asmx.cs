
namespace Task3
{
	using System;
	using System.Data;
	using System.Collections.Generic;
	using System.IO;
	using System.Web;
	using System.Web.Services;
	using Mono.Data.Sqlite;
	using Task3;

	public class MyItem
	{
		int item_id;
		string item_descr;
		double item_cost;

		public MyItem(int id, string descr, double cost)
		{
			item_id = id;
			item_descr = descr;
			item_cost = cost;
		}

		public int ID
		{
			get { return this.item_id; }
			set {this.item_id = value; }
		}

		public string DESCR
		{
			get { return this.item_descr; }
		}

		public double COST
		{
			get { return this.item_cost; }
		}
	}

	public class webservice : System.Web.Services.WebService
	{
		public webservice()
		{	
		}

		[WebMethod]
		public void Init()
		{
			string connectionstring = @"URI=file:Item.db";
			string dbFile = @"Item.db";
		
			if (!File.Exists (dbFile)) {
				SqliteConnection con = new SqliteConnection (connectionstring);
				con.Open ();
				SqliteCommand cmd = con.CreateCommand ();
				string sql = @"CREATE TABLE items ( id INTEGER PRIMARY KEY AUTOINCREMENT, " +
					"item VARCHAR(20) NOT NULL, " +
					" cost REAL NOT NULL);";
				cmd.CommandText = sql;
				cmd.ExecuteNonQuery ();
				cmd.Dispose ();
				con.Close ();
			}
		}

		public void AddItem(string item, double cost)
		{
			string connectionstring = @"URI=file:Item.db";
			SqliteConnection sqcon = new SqliteConnection (connectionstring);
			sqcon.Open ();
			SqliteCommand sqcmd = sqcon.CreateCommand ();

			string sql = @"INSERT INTO items(item, cost) VALUES(@item, @cost);";
			sqcmd.CommandText = sql;
			sqcmd.Prepare ();

			sqcmd.Parameters.AddWithValue ("@item", item);
			sqcmd.Parameters.AddWithValue ("@cost", cost);
			sqcmd.ExecuteNonQuery ();

			sqcmd.Dispose ();
			sqcon.Clone ();

			Console.WriteLine ("item {0} successfully added!", item);
		}

		public void RemoveItem(int id)
		{
			string connectionstring = @"URI=file:Item.db";
			SqliteConnection sqcon = new SqliteConnection (connectionstring);
			sqcon.Open ();
			SqliteCommand sqcmd = sqcon.CreateCommand ();

			string sql = @"DELETE FROM items WHERE id = @idno;";
			sqcmd.CommandText = sql;
			sqcmd.Prepare ();

			sqcmd.Parameters.AddWithValue ("@idno", id);
			sqcmd.ExecuteNonQuery ();
			sqcmd.Dispose ();
			sqcon.Clone ();

			Console.WriteLine ("item No {0} successfully removed!", id);
		}

		public void ShowItem()
		{
			List<MyItem> item_list = new List<MyItem> ();
			double total_cost = 0;
			string connectionstring = @"URI=file:Item.db";
			SqliteConnection sqcon = new SqliteConnection (connectionstring);
			sqcon.Open ();
			SqliteCommand sqcmd = sqcon.CreateCommand ();

			string sql = @"SELECT id, item, cost from items";
			sqcmd.CommandText = sql;
			IDataReader reader = sqcmd.ExecuteReader ();

			while(reader.Read())
				item_list.Add(new MyItem(reader.GetInt32(0), reader.GetString(1), reader.GetDouble(2)));

			sqcmd.Dispose ();
			sqcon.Clone ();

			foreach (MyItem i in item_list) {
				Console.WriteLine ("item No {0} {1} costs {2}", i.ID, i.DESCR, i.COST);
				total_cost += i.COST;
				Console.WriteLine ("TotalCost: {0}", total_cost);
			}
		}
	}
}

