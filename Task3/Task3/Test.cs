using System;

namespace Task3
{
	public class Test
	{
		public static void Main(string [] args)
		{
			string descr;
			int id;
			double cost;
			int answ;

			webservice w = new webservice ();
			w.Init ();

			do {
				Console.WriteLine ("\n MENU\n\n1.Add Item\n2.Delete Item\n3.Show Item\n\n Press[1-3]:");
				answ = int.Parse (Console.ReadLine ());
				switch (answ) {
				case 1:
					Console.WriteLine ("Please enter a short descr and the cost of the item: ");
					Console.WriteLine ("Descr:");
					descr = Console.ReadLine ();
					Console.WriteLine ("Cost: ");
					cost = double.Parse (Console.ReadLine ());
					w.AddItem (descr, cost);
					break;
				case 2:
					Console.WriteLine ("Please which item you want to delete: ");
					w.ShowItem ();
					Console.WriteLine ("ID: ");
					id = int.Parse (Console.ReadLine());
					w.RemoveItem (id);
					break;
				case 3:
					w.ShowItem ();
					break;
				default:
					return;
				}
			} while(answ < 4);

		}
	}
}

