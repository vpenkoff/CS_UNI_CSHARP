using System;

namespace Task1
{
	class Rational : IComparable
	{
		int nominator;
		int denominator;

		public Rational(int n, int d)
		{
			try
			{
				if ((d == 0) || ((n < 0) && (d < 0)))
					throw new RationalException("Invalid Arguments");
				nominator = n;
				denominator = d;
				this.reduce ();
			}
			catch (RationalException ex)
			{
				Console.WriteLine("{0} Exception cought", ex);
			}
		}

		public Rational()
		{
			try {
				try {
					Console.WriteLine("Enter a nominator: "); 
					nominator = int.Parse(Console.ReadLine());
					Console.WriteLine("Enter a dominator: ");
					denominator = int.Parse(Console.ReadLine());
				} catch (SystemException ex) {
					Console.WriteLine ("Rational exception handled: {0}, {1}", ex.GetType (), ex.Message);
				}
				if ((denominator == 0) || ((nominator < 0) && (denominator < 0)))
					throw new RationalException("Arguments are invalid");
				this.reduce ();
			}
			catch(RationalException ex) {
				throw ex;
			}
		}

		public static Rational operator +(Rational r1, Rational r2)
		{
			/* a/b + c/d = (ad + bc)/(b*d) */
			int new_nominator = ((r1.nominator * r2.denominator) + (r2.nominator * r1.denominator));
			int new_denominator = (r1.denominator * r2.denominator);
			return new Rational(new_nominator, new_denominator);
		}

		public static Rational operator -(Rational r1, Rational r2)
		{
			/* a/b - c/d = (ad - bc)/(b*d) */
			int new_nominator = ((r1.nominator * r2.denominator) - (r2.nominator * r1.denominator));
			int new_denominator = (r1.denominator * r2.denominator);
			return new Rational(new_nominator, new_denominator);
		}

		public static Rational operator *(Rational r1, Rational r2)
		{
			/* a/b * c/d = ac/bd */
			int new_nominator = (r1.nominator * r2.nominator);
			int new_denominator = (r1.denominator * r2.denominator);
			return new Rational(new_nominator, new_denominator);
		}

		public static Rational operator /(Rational r1, Rational r2)
		{
			/* a/b / c/d = a*d/b*c */
			int new_nominator = (r1.nominator * r2.denominator);
			                     int new_denominator = (r1.denominator * r2.nominator);
			                     return new Rational(new_nominator, new_denominator);
		}

		public static Boolean operator ==(Rational r1, Rational r2)
		{
				if ((r1.nominator == r2.nominator) && (r1.denominator == r2.denominator))
					return true;
				else
					return false;
		}

		public static Boolean operator !=(Rational r1, Rational r2)
		{
				if ((r1.nominator == r2.nominator) && (r1.denominator == r2.denominator))
					return false;
				else
					return true;
		}

		public static Boolean operator <(Rational r1, Rational r2)
		{
				if (((r1.nominator * r2.denominator) / (r1.denominator * r2.denominator)) < ((r2.nominator * r1.denominator) / (r1.denominator * r2.denominator)))
					return true;
				else
					return false;
		}

		public static Boolean operator >(Rational r1, Rational r2)
		{
				if (((r1.nominator * r2.denominator) / (r1.denominator * r2.denominator)) > ((r2.nominator * r1.denominator) / (r1.denominator * r2.denominator)))
					return true;
				else
					return false;
		}

		public override string ToString()
		{
				return (nominator + "/" + denominator);
		}

		public override bool Equals(object obj)
		{
			if (obj == null)
					return false;

				Rational r = obj as Rational;
				if ((System.Object)r == null)
					return false;

			return (this.nominator == r.nominator) && (this.denominator == r.denominator);
		}

		public override int GetHashCode()
		{
				return Tuple.Create(nominator, denominator).GetHashCode();
		}

		public int CompareTo(object obj)
		{
			if (obj == null)
				return 1;
			Rational otherRational = obj as Rational;

			if (this.nominator == otherRational.nominator)
				return this.denominator.CompareTo(otherRational.denominator);

			return this.nominator.CompareTo(otherRational.nominator);
		}
					
		/* Recursive version of the Euclidean Algorithm for GCD.
		 * See http://en.wikipedia.org/wiki/Euclidean_algorithm
		*/

		private int gcd(int n1, int n2)
		{
			if (n2 == 0)
				return n1;
			else
				return gcd (n2, (n1 % n2));
		}

		private void reduce()
		{	
			int common = gcd (Math.Abs(nominator), denominator);
			nominator /= common;
			denominator /= common;
		}
	}

	class RationalException : Exception
	{
		public RationalException() : base() { }
		public RationalException(String message) : base(message) { }

	}

	class MainClass
	{
		public static void Calculate(Rational r1, Rational r2, string op)
		{
			try {
			if (op.Contains ("+")) {
				Console.WriteLine("{0} + {1} = {2}",r1, r2, (r1 + r2));
				}
			else if (op.Contains ("-")) {
				Console.WriteLine("{0} - {1} = {2}",r1, r2, (r1 - r2));
				}
			else if (op.Contains ("*")) {
				Console.WriteLine("{0} * {1} = {2}",r1, r2, (r1 * r2));
				}
			else if (op.Contains ("/")) {
				Console.WriteLine("{0} / {1} = {2}",r1, r2, (r1 / r2));
				}
			else if (op.Contains ("==")) {
					if (r1.Equals(r2)) {
						Console.WriteLine("{0} equals {1}", r1, r2);
					}
					else
					Console.WriteLine("{0} not equals {1}", r1, r2);
				}
			else if (op.Contains ("!=")) {
					if (!r1.Equals(r2))
						Console.WriteLine("{0} not equals {1}", r1, r2);
					else 
						Console.WriteLine("{0} equals {1}", r1, r2);
				}
			else if (op.Contains ("<")) {
					if (r1.CompareTo(r2) == -1)
						Console.WriteLine("True");
					else
						Console.WriteLine("False");
				}
			else if (op.Contains (">")) {
					if (r1.CompareTo(r2) == 1)
						Console.WriteLine("True");
					else
						Console.WriteLine("False");
				}
			else
				throw new RationalException("Invalid Operation");
			}
			catch (RationalException ex)
			{
				throw ex;
			}
		}

		public static void Main (string[] args)
		{
			Rational[] num_array = new Rational[3]; 
			try {
				Rational left = new Rational ();
				Rational right = new Rational ();
				Calculate (left, right, "+");
				Calculate (left, right, "-");
				Calculate (left, right, "*");
				Calculate (left, right, "/");
				Calculate (left, right, "==");
				Calculate (left, right, "!=");
				Calculate (left, right, ">");
				Calculate (left, right, "<");
			}
			catch (RationalException e) {
				Console.WriteLine ("Rational exception handled: {0}, {1}", e.GetType (), e.Message);
			}

			try {
				Console.WriteLine ("Please enter some nums to sort: ");
				for (int i = 0; i < num_array.Length; i++)
					num_array [i] = new Rational ();

				Array.Sort (num_array, new Comparison<Rational> ((r1, r2) => r2.CompareTo (r1)));
				Console.WriteLine ("The entered rationals in ascending order: ");
				foreach (Rational r in num_array)
					Console.WriteLine ("{0}", r.ToString());
			} catch (RationalException e) {
				Console.WriteLine ("Rational exception handled: {0}, {1}", e.GetType (), e.Message);
			}
		}
	}
}
